import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
//services
import { SignService } from './login/sign.service';
import { GeneralService } from 'src/app/principal/general.service';
var AppComponent = /** @class */ (function () {
    function AppComponent(signService, generalService) {
        this.signService = signService;
        this.generalService = generalService;
        this.title = 'CMS-v2';
        this.mostrarMenu = false;
    }
    AppComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.signService.mostrarMenuEmitter.subscribe(function (mostrar) { return _this.mostrarMenu = mostrar; });
    };
    AppComponent.prototype.ngAfterContentChecked = function () {
        // this.backgroundBody = this.generalService.backgroundBody;
        // this.backgroundColorBody = this.generalService.backgroundColorBody;
    };
    AppComponent = tslib_1.__decorate([
        Component({
            selector: 'app-root',
            templateUrl: './app.component.html',
            styleUrls: ['./app.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [SignService, GeneralService])
    ], AppComponent);
    return AppComponent;
}());
export { AppComponent };
//# sourceMappingURL=app.component.js.map