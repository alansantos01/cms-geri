import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import * as $ from 'jquery';
var GeneralService = /** @class */ (function () {
    function GeneralService() {
        this.username = 'alansantos';
        this.password = '123alan';
    }
    GeneralService.prototype.showSubMenu = function (identificador) {
        this.sub_menu = true;
        this.menu = identificador;
    };
    GeneralService.prototype.hideSubMenu = function () {
        this.sub_menu = false;
        this.menu = "";
    };
    GeneralService.prototype.onselect = function (valor) {
        this.select = valor;
    };
    GeneralService.prototype.abrirModal = function (modal) {
        this.modalAtivo = modal;
    };
    GeneralService.prototype.abas = function (aba_v) {
        this.abas_select = aba_v;
    };
    GeneralService.prototype.background = function (bgBody) {
        this.backgroundBody = bgBody;
    };
    GeneralService.prototype.background_color = function (bgColorBody) {
        this.backgroundColorBody = bgColorBody;
    };
    GeneralService.prototype.nmArquivo = function (id) {
        var alvo = $(id);
        var alvo_next = $(alvo).next();
        var inputNome = $(alvo).prev();
        var inputFicheiro = alvo_next;
        $(inputFicheiro).on('change', function () {
            var nome = $(inputFicheiro).val().split("\\").pop();
            $(inputNome).text(nome);
            $(inputNome).html(nome);
        });
    };
    ;
    GeneralService.prototype.previewImg = function (imagem) {
        var labelLocation = imagem;
        var inputFileLocation = $(labelLocation).next();
        $(inputFileLocation).on('change', function () {
            var imgItem = $(this)[0].files;
            var imgCount = $(this)[0].files.length;
            var imgPath = $(this)[0].value;
            var imgExt = imgPath.substring(imgPath.lastIndexOf('.') + 1).toLowerCase();
            var imagePreview = $(".imgpreview");
            var labelImage = imagem;
            var imagePreview2 = $(labelImage).closest(".x-fileLabel");
            var imagePreview3 = $(imagePreview2).find(".x-imagePreview");
            imagePreview.empty();
            if (imgExt == "gif" || imgExt == "png" || imgExt == "jpg" || imgExt == "jpeg" || imgExt == "bmp") {
                if (typeof (FileReader) != "undefined") {
                    for (var i = 0; i < imgCount; i++) {
                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var fileUrl = event.target.result;
                            $("<img />", {
                                "src": fileUrl,
                                "width": "150",
                                "class": "imgClass"
                            }).appendTo(imagePreview3);
                        };
                        imagePreview2.show();
                        reader.readAsDataURL($(this)[0].files[i]);
                    }
                }
                else {
                    imagePreview2.html("Browser ini tidak mendukung FileReader");
                }
            }
            else {
                imagePreview2.html("File harus format gambar");
            }
        });
    };
    GeneralService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], GeneralService);
    return GeneralService;
}());
export { GeneralService };
//# sourceMappingURL=general.service.js.map