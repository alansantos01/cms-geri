import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
//Modules
import { ConteudoModule } from '../components/destaque/conteudo/conteudo.module';
import { CommonsModule } from '../commons/commons.module';
import { ModaisModule } from '../modais/modais.module';
import { MenuModule } from '../components/menus/menu.module';
var SystemModule = /** @class */ (function () {
    function SystemModule() {
    }
    SystemModule = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: [
                CommonModule,
                ConteudoModule,
                CommonsModule,
                ModaisModule,
                MenuModule
            ],
            exports: [
                CommonModule,
                ConteudoModule,
                CommonsModule,
                ModaisModule,
                MenuModule
            ]
        })
    ], SystemModule);
    return SystemModule;
}());
export { SystemModule };
//# sourceMappingURL=system.module.js.map