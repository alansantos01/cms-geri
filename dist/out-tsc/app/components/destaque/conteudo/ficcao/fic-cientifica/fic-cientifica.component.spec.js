import { async, TestBed } from '@angular/core/testing';
import { FicCientificaComponent } from './fic-cientifica.component';
describe('FicCientificaComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [FicCientificaComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(FicCientificaComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=fic-cientifica.component.spec.js.map