import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var FicCientificaComponent = /** @class */ (function () {
    function FicCientificaComponent(generalService) {
        this.generalService = generalService;
    }
    FicCientificaComponent.prototype.ngOnInit = function () {
    };
    FicCientificaComponent.prototype.abrirAba = function (aba_v) {
        this.generalService.abas(aba_v);
    };
    FicCientificaComponent.prototype.nmArquivo = function (file) {
        this.generalService.nmArquivo(file);
    };
    FicCientificaComponent.prototype.previewImg = function (imagem) {
        this.generalService.previewImg(imagem);
    };
    FicCientificaComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    FicCientificaComponent.prototype.ngAfterContentChecked = function () {
        this.select = this.generalService.select;
        this.aba = this.generalService.abas_select;
        this.modalAtivo = this.generalService.modalAtivo;
    };
    FicCientificaComponent = tslib_1.__decorate([
        Component({
            selector: 'app-fic-cientifica',
            templateUrl: './fic-cientifica.component.html',
            styleUrls: ['./fic-cientifica.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], FicCientificaComponent);
    return FicCientificaComponent;
}());
export { FicCientificaComponent };
//# sourceMappingURL=fic-cientifica.component.js.map