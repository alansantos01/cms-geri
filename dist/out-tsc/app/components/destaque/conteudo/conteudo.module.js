import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { WhiteLabelComponent } from './criar-paginas/white-label/white-label.component';
import { LandingPagesComponent } from './criar-paginas/landing-pages/landing-pages.component';
import { StaticPagesComponent } from './criar-paginas/static-pages/static-pages.component';
import { ConfiguracoesComponent } from './configuracoes-page/configuracoes/configuracoes.component';
import { ConteudoComponent } from './conteudo.component';
import { MenuModule } from '../../menus/menu.module';
import { FicCientificaComponent } from './ficcao/fic-cientifica/fic-cientifica.component';
var ConteudoModule = /** @class */ (function () {
    function ConteudoModule() {
    }
    ConteudoModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                WhiteLabelComponent,
                LandingPagesComponent,
                StaticPagesComponent,
                ConfiguracoesComponent,
                ConteudoComponent,
                FicCientificaComponent
            ],
            exports: [
                ConteudoComponent
            ],
            imports: [
                CommonModule,
                MenuModule
            ]
        })
    ], ConteudoModule);
    return ConteudoModule;
}());
export { ConteudoModule };
//# sourceMappingURL=conteudo.module.js.map