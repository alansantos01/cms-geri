import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var ConteudoComponent = /** @class */ (function () {
    function ConteudoComponent(generalService) {
        this.generalService = generalService;
    }
    ConteudoComponent.prototype.ngOnInit = function () {
    };
    ConteudoComponent.prototype.show = function (item) {
        this.generalService.showSubMenu(item);
    };
    ConteudoComponent.prototype.hide = function () {
        this.generalService.hideSubMenu();
    };
    ConteudoComponent = tslib_1.__decorate([
        Component({
            selector: 'app-conteudo',
            templateUrl: './conteudo.component.html',
            styleUrls: ['./conteudo.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], ConteudoComponent);
    return ConteudoComponent;
}());
export { ConteudoComponent };
//# sourceMappingURL=conteudo.component.js.map