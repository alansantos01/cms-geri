import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var StaticPagesComponent = /** @class */ (function () {
    function StaticPagesComponent() {
    }
    StaticPagesComponent.prototype.ngOnInit = function () {
    };
    StaticPagesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-static-pages',
            templateUrl: './static-pages.component.html',
            styleUrls: ['./static-pages.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], StaticPagesComponent);
    return StaticPagesComponent;
}());
export { StaticPagesComponent };
//# sourceMappingURL=static-pages.component.js.map