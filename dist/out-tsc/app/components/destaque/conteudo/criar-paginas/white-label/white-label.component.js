import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var WhiteLabelComponent = /** @class */ (function () {
    function WhiteLabelComponent(generalService) {
        this.generalService = generalService;
    }
    WhiteLabelComponent.prototype.ngOnInit = function () {
    };
    WhiteLabelComponent.prototype.abrirAba = function (aba_v) {
        this.generalService.abas(aba_v);
    };
    WhiteLabelComponent.prototype.nmArquivo = function (file) {
        this.generalService.nmArquivo(file);
    };
    WhiteLabelComponent.prototype.previewImg = function (imagem) {
        this.generalService.previewImg(imagem);
    };
    WhiteLabelComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    WhiteLabelComponent.prototype.ngAfterContentChecked = function () {
        this.select = this.generalService.select;
        this.aba = this.generalService.abas_select;
        this.modalAtivo = this.generalService.modalAtivo;
    };
    WhiteLabelComponent = tslib_1.__decorate([
        Component({
            selector: 'app-white-label',
            templateUrl: './white-label.component.html',
            styleUrls: ['./white-label.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], WhiteLabelComponent);
    return WhiteLabelComponent;
}());
export { WhiteLabelComponent };
//# sourceMappingURL=white-label.component.js.map