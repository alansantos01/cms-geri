import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var LandingPagesComponent = /** @class */ (function () {
    function LandingPagesComponent() {
    }
    LandingPagesComponent.prototype.ngOnInit = function () {
    };
    LandingPagesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-landing-pages',
            templateUrl: './landing-pages.component.html',
            styleUrls: ['./landing-pages.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], LandingPagesComponent);
    return LandingPagesComponent;
}());
export { LandingPagesComponent };
//# sourceMappingURL=landing-pages.component.js.map