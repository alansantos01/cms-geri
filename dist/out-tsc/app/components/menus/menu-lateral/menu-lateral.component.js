import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var MenuLateralComponent = /** @class */ (function () {
    function MenuLateralComponent(generalService) {
        this.generalService = generalService;
    }
    MenuLateralComponent.prototype.ngOnInit = function () {
    };
    MenuLateralComponent.prototype.show = function (item) {
        this.generalService.showSubMenu(item);
    };
    MenuLateralComponent.prototype.hide = function () {
        this.generalService.hideSubMenu();
    };
    MenuLateralComponent.prototype.onselect = function (valor) {
        this.generalService.onselect(valor);
    };
    MenuLateralComponent.prototype.ngAfterContentChecked = function () {
        this.sub_menu = this.generalService.sub_menu;
        this.menu = this.generalService.menu;
        this.modalAtivo = this.generalService.modalAtivo;
    };
    MenuLateralComponent = tslib_1.__decorate([
        Component({
            selector: 'app-menu-lateral',
            templateUrl: './menu-lateral.component.html',
            styleUrls: ['./menu-lateral.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], MenuLateralComponent);
    return MenuLateralComponent;
}());
export { MenuLateralComponent };
//# sourceMappingURL=menu-lateral.component.js.map