import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var SubMenuComponent = /** @class */ (function () {
    function SubMenuComponent(generalService) {
        this.generalService = generalService;
    }
    SubMenuComponent.prototype.ngOnInit = function () {
    };
    SubMenuComponent.prototype.onselect = function (valor) {
        this.generalService.onselect(valor);
    };
    SubMenuComponent.prototype.ngAfterContentChecked = function () {
        this.sub_menu = this.generalService.sub_menu;
        this.menu = this.generalService.menu;
        this.select = this.generalService.select;
    };
    SubMenuComponent = tslib_1.__decorate([
        Component({
            selector: 'app-sub-menu',
            templateUrl: './sub-menu.component.html',
            styleUrls: ['./sub-menu.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], SubMenuComponent);
    return SubMenuComponent;
}());
export { SubMenuComponent };
//# sourceMappingURL=sub-menu.component.js.map