import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MenuLateralComponent } from './menu-lateral/menu-lateral.component';
import { SubMenuComponent } from './menu-lateral/sub-menu/sub-menu.component';
var MenuModule = /** @class */ (function () {
    function MenuModule() {
    }
    MenuModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                MenuLateralComponent,
                SubMenuComponent
            ],
            exports: [
                MenuLateralComponent,
                SubMenuComponent
            ],
            imports: [
                CommonModule
            ]
        })
    ], MenuModule);
    return MenuModule;
}());
export { MenuModule };
//# sourceMappingURL=menu.module.js.map