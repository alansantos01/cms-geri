import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import { Validators, FormBuilder } from "@angular/forms";
//Classes
import { Usuario } from './class/usuario';
//Services
import { SignService } from './sign.service';
var LoginComponent = /** @class */ (function () {
    function LoginComponent(generalService, signService, formBuilder) {
        this.generalService = generalService;
        this.signService = signService;
        this.formBuilder = formBuilder;
        this.username = new Usuario();
        this.formulario = this.formBuilder.group({
            email: [null, [Validators.email, Validators.required]],
            senha: [null, [Validators.minLength(6), Validators.required, Validators.maxLength(20)]]
        });
    }
    LoginComponent.prototype.verificaValidTouched = function (campo) {
        return (this.formulario.get(campo).invalid && (this.formulario.get(campo).touched || this.formulario.get(campo).dirty)) ? 'has-erro has-feedback' : '';
    };
    LoginComponent.prototype.ngOnInit = function () {
        window.sessionStorage.setItem('logado', 'false');
        this.generalService.background("url('background.jpg')");
        this.generalService.background_color("#9b59b6");
    };
    LoginComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    LoginComponent.prototype.fazerLogin = function () {
        this.signService.fazerLogin(this.username);
    };
    LoginComponent.prototype.ngAfterContentChecked = function () {
        this.modalAtivo = this.generalService.modalAtivo;
        // this.username = this.generalService.username;
        // this.password = this.generalService.password;
        this.logado = this.generalService.logado;
    };
    LoginComponent = tslib_1.__decorate([
        Component({
            selector: 'app-login',
            templateUrl: './login.component.html',
            styleUrls: ['./login.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService, SignService, FormBuilder])
    ], LoginComponent);
    return LoginComponent;
}());
export { LoginComponent };
//# sourceMappingURL=login.component.js.map