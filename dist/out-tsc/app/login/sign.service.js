import * as tslib_1 from "tslib";
import { GeneralService } from 'src/app/principal/general.service';
import { Injectable, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
var SignService = /** @class */ (function () {
    function SignService(router, generalService) {
        this.router = router;
        this.generalService = generalService;
        this.mostrarMenuEmitter = new EventEmitter();
        this.usuarioAutenticado = false;
    }
    SignService.prototype.fazerLogin = function (usuario) {
        if (usuario.nome === 'usuario@email.com' && usuario.senha === '123456') {
            this.usuarioAutenticado = true;
            this.mostrarMenuEmitter.emit(true);
            this.router.navigate(['/home']);
            this.generalService.background("none");
            this.generalService.background_color("#333");
        }
        else {
            this.usuarioAutenticado = false;
            this.mostrarMenuEmitter.emit(false);
            alert('Usuario não encontrado');
        }
    };
    SignService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [Router, GeneralService])
    ], SignService);
    return SignService;
}());
export { SignService };
//# sourceMappingURL=sign.service.js.map