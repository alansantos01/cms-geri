import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
var CommonsModule = /** @class */ (function () {
    function CommonsModule() {
    }
    CommonsModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                HeaderComponent,
                FooterComponent
            ],
            exports: [
                HeaderComponent,
                FooterComponent
            ],
            imports: [
                CommonModule
            ]
        })
    ], CommonsModule);
    return CommonsModule;
}());
export { CommonsModule };
//# sourceMappingURL=commons.module.js.map