import * as tslib_1 from "tslib";
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
// Components
import { AppComponent } from './app.component';
//Services
import { GeneralService } from './principal/general.service';
//Routes
import { RoutesRoutingModule } from './routes/routes-routing.module';
//Modules
import { LoginComponent } from './login/login.component';
import { SystemModule } from './components/system.module';
import { SignService } from './login/sign.service';
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                AppComponent,
                LoginComponent
            ],
            imports: [
                BrowserModule,
                SystemModule,
                RoutesRoutingModule,
                FormsModule,
                ReactiveFormsModule,
                NbButtonModule
            ],
            providers: [GeneralService, SignService],
            bootstrap: [AppComponent]
        })
    ], AppModule);
    return AppModule;
}());
export { AppModule };
//# sourceMappingURL=app.module.js.map