import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RoutesRoutingModule } from './routes-routing.module';
var RoutesModule = /** @class */ (function () {
    function RoutesModule() {
    }
    RoutesModule = tslib_1.__decorate([
        NgModule({
            declarations: [],
            imports: [
                CommonModule,
                RoutesRoutingModule
            ]
        })
    ], RoutesModule);
    return RoutesModule;
}());
export { RoutesModule };
//# sourceMappingURL=routes.module.js.map