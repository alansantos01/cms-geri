import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { LoginComponent } from '../login/login.component';
import { ConteudoComponent } from '../components/destaque/conteudo/conteudo.component';
var routes = [
    { path: '', component: LoginComponent },
    { path: 'home', component: ConteudoComponent }
];
var RoutesRoutingModule = /** @class */ (function () {
    function RoutesRoutingModule() {
    }
    RoutesRoutingModule = tslib_1.__decorate([
        NgModule({
            imports: [RouterModule.forRoot(routes)],
            exports: [RouterModule]
        })
    ], RoutesRoutingModule);
    return RoutesRoutingModule;
}());
export { RoutesRoutingModule };
//# sourceMappingURL=routes-routing.module.js.map