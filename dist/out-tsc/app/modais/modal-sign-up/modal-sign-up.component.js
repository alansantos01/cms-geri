import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var ModalSignUpComponent = /** @class */ (function () {
    function ModalSignUpComponent(generalService) {
        this.generalService = generalService;
    }
    ModalSignUpComponent.prototype.ngOnInit = function () {
    };
    ModalSignUpComponent.prototype.abrirAba = function (aba_v) {
        this.generalService.abas(aba_v);
    };
    ModalSignUpComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    ModalSignUpComponent.prototype.ngAfterContentChecked = function () {
        this.modalAtivo = this.generalService.modalAtivo;
    };
    ModalSignUpComponent = tslib_1.__decorate([
        Component({
            selector: 'app-modal-sign-up',
            templateUrl: './modal-sign-up.component.html',
            styleUrls: ['./modal-sign-up.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], ModalSignUpComponent);
    return ModalSignUpComponent;
}());
export { ModalSignUpComponent };
//# sourceMappingURL=modal-sign-up.component.js.map