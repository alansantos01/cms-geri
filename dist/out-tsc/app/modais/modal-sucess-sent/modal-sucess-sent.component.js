import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var ModalSucessSentComponent = /** @class */ (function () {
    function ModalSucessSentComponent(generalService) {
        this.generalService = generalService;
    }
    ModalSucessSentComponent.prototype.ngOnInit = function () {
    };
    ModalSucessSentComponent.prototype.abrirAba = function (aba_v) {
        this.generalService.abas(aba_v);
    };
    ModalSucessSentComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    ModalSucessSentComponent.prototype.ngAfterContentChecked = function () {
        this.modalAtivo = this.generalService.modalAtivo;
    };
    ModalSucessSentComponent = tslib_1.__decorate([
        Component({
            selector: 'app-modal-sucess-sent',
            templateUrl: './modal-sucess-sent.component.html',
            styleUrls: ['./modal-sucess-sent.component.less']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], ModalSucessSentComponent);
    return ModalSucessSentComponent;
}());
export { ModalSucessSentComponent };
//# sourceMappingURL=modal-sucess-sent.component.js.map