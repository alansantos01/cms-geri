import { async, TestBed } from '@angular/core/testing';
import { ModalSucessSentComponent } from './modal-sucess-sent.component';
describe('ModalSucessSentComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ModalSucessSentComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ModalSucessSentComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=modal-sucess-sent.component.spec.js.map