import * as tslib_1 from "tslib";
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ModalSucessSaveComponent } from './modal-sucess-save/modal-sucess-save.component';
import { ModalSignUpComponent } from './modal-sign-up/modal-sign-up.component';
import { ModalSucessSentComponent } from './modal-sucess-sent/modal-sucess-sent.component';
var ModaisModule = /** @class */ (function () {
    function ModaisModule() {
    }
    ModaisModule = tslib_1.__decorate([
        NgModule({
            declarations: [
                ModalSucessSaveComponent,
                ModalSignUpComponent,
                ModalSucessSentComponent
            ],
            exports: [
                ModalSucessSaveComponent,
                ModalSignUpComponent,
                ModalSucessSentComponent
            ],
            imports: [
                CommonModule
            ]
        })
    ], ModaisModule);
    return ModaisModule;
}());
export { ModaisModule };
//# sourceMappingURL=modais.module.js.map