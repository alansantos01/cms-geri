import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
var ModalSucessSaveComponent = /** @class */ (function () {
    function ModalSucessSaveComponent(generalService) {
        this.generalService = generalService;
    }
    ModalSucessSaveComponent.prototype.ngOnInit = function () {
    };
    ModalSucessSaveComponent.prototype.abrirAba = function (aba_v) {
        this.generalService.abas(aba_v);
    };
    ModalSucessSaveComponent.prototype.nmArquivo = function (file) {
        this.generalService.nmArquivo(file);
    };
    ModalSucessSaveComponent.prototype.previewImg = function (imagem) {
        this.generalService.previewImg(imagem);
    };
    ModalSucessSaveComponent.prototype.abrirModal = function (modal) {
        this.generalService.abrirModal(modal);
    };
    ModalSucessSaveComponent.prototype.ngAfterContentChecked = function () {
        this.modalAtivo = this.generalService.modalAtivo;
    };
    ModalSucessSaveComponent = tslib_1.__decorate([
        Component({
            selector: 'app-modal-sucess-save',
            templateUrl: './modal-sucess-save.component.html',
            styleUrls: ['./modal-sucess-save.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [GeneralService])
    ], ModalSucessSaveComponent);
    return ModalSucessSaveComponent;
}());
export { ModalSucessSaveComponent };
//# sourceMappingURL=modal-sucess-save.component.js.map