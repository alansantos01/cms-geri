import { async, TestBed } from '@angular/core/testing';
import { ModalSucessSaveComponent } from './modal-sucess-save.component';
describe('ModalSucessSaveComponent', function () {
    var component;
    var fixture;
    beforeEach(async(function () {
        TestBed.configureTestingModule({
            declarations: [ModalSucessSaveComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = TestBed.createComponent(ModalSucessSaveComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=modal-sucess-save.component.spec.js.map