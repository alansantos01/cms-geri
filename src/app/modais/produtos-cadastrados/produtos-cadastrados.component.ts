import { Component, OnInit, AfterContentChecked } from '@angular/core';

//Service
import { GeneralService } from 'src/app/principal/general.service';
import { CadastrarCardapioService } from 'src/app/services/cadastrar-cardapio.service';

@Component({
  selector: 'app-produtos-cadastrados',
  templateUrl: './produtos-cadastrados.component.html',
  styleUrls: ['./produtos-cadastrados.component.scss']
})
export class ProdutosCadastradosComponent implements OnInit, AfterContentChecked {
  public modalAtivo: string
  public cardapio: Array<object> = []

  constructor(private generalService: GeneralService, private cadastrarCardapio: CadastrarCardapioService) { }

  ngOnInit() {
  }
  
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  ngAfterContentChecked(){
    this.modalAtivo = this.generalService.modalAtivo
    this.cardapio = this.cadastrarCardapio.cardapio
  }
}
