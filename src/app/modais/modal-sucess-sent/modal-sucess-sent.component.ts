import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-modal-sucess-sent',
  templateUrl: './modal-sucess-sent.component.html',
  styleUrls: ['./modal-sucess-sent.component.less']
})
export class ModalSucessSentComponent implements OnInit, AfterContentChecked {
  public select: string;
  public modalAtivo: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  ngAfterContentChecked(){
    this.modalAtivo = this.generalService.modalAtivo;
  }
}