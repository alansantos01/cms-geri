export class TemplateDefault {
    html: string;
    css: string;
    js?: string;
}
