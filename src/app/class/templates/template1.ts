import { TemplateDefault } from "./templateDefault/template-default";
import { CadastrarDadosService } from "../../services/cadastrarDados/cadastrar-dados.service";

var data: CadastrarDadosService;

export const template1: TemplateDefault[] = [
    {
        html: `<!DOCTYPE html>
        <html lang="pt-br">
            <head>
                <link href="css/estilo.css" rel="stylesheet" type="text/css">
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
                <meta name="description" content="">
                <title>${data.template.metatags.title}</title>
                <meta name="keywords" content="">
                <base href="">
                <meta name="robots" content="index,follow">
                <meta name="rating" content="General">
                <meta name="revisit-after" content="7 days">
                <meta name="author" content="Geri System">
                <meta name="viewport" content="width=600">
            </head>
            <body>
                <div class="painel">
                    <header>
                        <img src="img/logo-fundo-cinza.jpg"/>
                        <div class="container">
                            <div class="d-flex">
                                <a href="https://www.instagram.com/tech3assistencia/" target="blank"><img class="img2 imgWhite" src="img/instagram.svg"></a>
                                <a href="https://www.facebook.com/tech3assistencia/" target="blank"><img class="img2 imgWhite" src="img/facebook.svg"></a>
                            </div>
                            <div class="d-flex address">
                                <h1>Endereço, Nº | Cidade, UF</h1>
                                <a href="" target="blank">
                                    <img class="img2" src="img/maps.png" >
                                </a>
                            </div>
                        </div>
                    </header>
                    <div class="text-painel">
                        Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
                    </div>
                </div>
                <div id="destaque">
                    <div class="aboutUs">
                        <div class="container">
                            <h1>Quem somos</h1>
                        </div>
                        <div class="container">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </p>
                        </div>
                    </div>
                    <div class="vantagens">
                        <div class="container">
                            <div class="block">
                                <span class="number">1.</span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                            <div class="block">
                                <span class="number">2.</span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                            <div class="block">
                                <span class="number">3.</span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="container">
                            <h2>Vantagens</h2>
                        </div>
                    </div>
                    <div class="formulario">
                        <div class="container">
                            <h3>Contate-nos</h3>
                        </div>
                        <div class="box-formulario">
                            <form action="enviar.php" method="POST">
                                <input type="text" name="nome" placeholder="Digite seu nome">
                                <input type="text" name="email" placeholder="Digite seu e-mail">
                                <input type="text" name="telefone" placeholder="Digite seu contato">
                                <textarea placeholder="Digite sua mensagem aqui..." name="mensagem"></textarea>
                                <input type="submit" value="ENVIAR">
                            </form>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <h1>&#169; Copyright empresa.com.br - Todos os direitos reservados - Geri</h1>
                </div>
            </body>
        </html>`,
        css: `@import url('https://fonts.googleapis.com/css?family=Roboto:100,400,700,900&display=swap');

        *{
            margin:0;
            padding:0;
            box-sizing: border-box;
            font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
        }
        body{
            margin:0;
            padding:0;
            display: flex;
            flex-direction: column;
        }
        .d-flex{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .imgWhite{
            filter: invert(100%) sepia(0%) saturate(406%) hue-rotate(229deg) brightness(116%) contrast(100%);
        }
        
        /* Cabeçalho - Header */
        header{
            width:100%;
            display: flex;
            justify-content: space-between;
            padding: 10px 20px;
            position: absolute;
            top: 0;
        }
        header img{
            width:250px;
            height:150px;
            margin-top:10px;
        }
        header .container{
            display: flex;
            flex-direction: column;
            justify-content: center;
        }
        header .container div:first-child{
            justify-content: right;
            flex-direction: row-reverse;
            margin-bottom: 15px;
        }
        header .img2{
            height:30px;
            width:30px;
            margin-top:5px;
            margin-left:10px;
        }
        header .d-flex{
            display: flex;
        }
        header .address{
            text-align: right;
            flex-direction: row-reverse;
            color: #fff;
        }
        
        /* Painel - Banner */
        .painel{
            background:#aaa;
            background-image:url(../img/fundo-cinza.jpg);
            width:100%;
            height:800px;
            background-position:top;
            background-blend-mode:multiply;
            background-attachment:fixed;
            background-size:cover;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
        }
        .text-painel{
            color:#fff;
            font-weight:bold;
            font-size:2.7rem;
            width: 70%;
            text-align: center;
        }
        #destaque{
            text-align:center;
        }
        
        /* Quem Somos */
        
        .aboutUs{
            display: flex;
        }
        
        .aboutUs .container{
            display: flex;
            width: 50%;
            align-items: center;
            justify-content: center;
            min-height: 400px;
            padding: 30px 0;
            text-indent: 30px;
            flex-direction: column;
        }
        
        .aboutUs .container:first-child{
            background:rgba(41, 128, 185,1.0);
        }
        
        .aboutUs .container p {
            max-width: 600px;
            padding: 10px;
            text-align: justify;
            line-height: 25px;
            color: #777;
        }
        
        .aboutUs h1{
            color: #fff;
        }
        
        /* Vantagens */
        .vantagens{
            display: flex;
        }
        
        .vantagens .container{
            display: flex;
            width: 50%;
            padding: 15px;
            align-items: center;
            justify-content: center;
            min-height: 400px;
        }
        
        .vantagens .container h2{
            color: #fff;
            font-size: 2em;
            font-weight: bold;
        }
        
        .vantagens .container:first-child{
            flex-direction: column;
        }
        
        .vantagens .container:last-child{
            background:#f39c12;
        }
        
        .vantagens .container .block{
            text-align: left;
            margin: 20px 0px;
            display: flex;
            align-items: center;
        }
        
        .vantagens .container .block p{
            max-width: 300px;
            color: #777;
        }
        
        .vantagens .container .block span{
            font-size: 3rem;
            font-weight: bold;
            color: #f39c12;
            margin-right: 20px;
        }
        
        /* Contato */
        .formulario{
            height:auto;
            width:100%;
            color:#fff;
            display: flex;
        }
        .formulario h3{
            font-size:25pt;
            margin-top:10px;
        }
        .formulario .container{
            width: 50%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            background:#2980b9;
        }
        .formulario input[type=text]{
            width:500px;
            height:35px;
            color:#999;
            border:none;
            padding:10px;
            margin:20px;
            outline: none;
        }
        .formulario input[type=submit]{
            padding:15px 100px 15px 100px;
            border:none;
            background:rgba(52, 152, 219,1.0);
            color:#fff;
            cursor:pointer;
        }
        .formulario input[type=submit]:hover{
            background:#f39c12;
        }
        .formulario textarea{
            width:500px;
            height:200px;
            margin:20px;
            border:none;
            padding:10px;
            color:#999;
            margin-top:0px;
            outline: none;
        }
        .box-formulario{
            width:540px;
            margin:0 auto;
            margin-top:30px;
            padding:20px 0 20px 0;
        }
        
        /* Footer */
        #footer{
            width:100%;
            background:#fdfdfd;
            text-align:center;
            color:#555;
            padding:15px 0px 10px;
            font-size:6pt;
        }
        
        /* Mobile */
        @media (max-width:600px){
            header{
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }
            header .container div:first-child{
                justify-content: center;
                margin-top: 25px;
            }
            .painel{
                height: 900px;
            }
            .text-painel{
                margin-top: 250px;
            }
            .vantagens{
                flex-direction: column-reverse;
            }
            .aboutUs{
                flex-direction: column;
            }
            .vantagens .container, .aboutUs .container{
                width: 100%;
                min-height: 0px;
                padding: 20px 30px;
            }
            .vantagens .container p, .aboutUs .container p{
                font-size: 1.6rem;
                line-height: 30px;
            }
            .vantagens .container .block p{
                max-width: 500px;
            }
            header, .painel, .formulario, #footer{
                max-width:610px;
            }
            .formulario input[type=text]{
                height: 70px;
                font-size: 2rem;
            }
            .formulario input[type=submit]{
                height: 70px;
                font-size: 2rem;
            }
            .formulario textarea{
                font-size: 2rem;
                height: 500px;
                margin-top: 25px;
            }
            header .img2{
                height:60px;
                width:60px;
            }
        }`
    },
    {
        html: `<!DOCTYPE html>
        <html lang="pt-br">
            <head>
                <link href="css/estilo.css" rel="stylesheet" type="text/css">
                <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
                <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
                <meta name="description" content="">
                <title>${data.template.metatags.title}</title>
                <meta name="keywords" content="">
                <base href="">
                <meta name="robots" content="index,follow">
                <meta name="rating" content="General">
                <meta name="revisit-after" content="7 days">
                <meta name="author" content="Geri System">
                <meta name="viewport" content="width=600">
            </head>
            <body>
                <header>
                    <img src="img/logo-fundo-cinza.jpg"/>
                    <div class="container">
                        <div class="d-flex">
                            <a href="" target="blank"><img class="img2 imgWhite" src="img/instagram.svg"></a>
                            <a href="" target="blank"><img class="img2 imgWhite" src="img/facebook.svg"></a>
                        </div>
                    </div>
                </header>
                <div class="painel">
                    <div class="text-painel">
                        Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum Lorem Ipsum
                    </div>
                </div>
                <div id="destaque">
                    <div class="aboutUs">
                        <div class="container">
                            <h1>Quem somos</h1>
                        </div>
                        <div class="container">
                            <p>
                                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                            </p>
                            <p>
                                Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?
                            </p>
                        </div>
                    </div>
                    <div class="vantagens">
                        <div class="container">
                            <div class="block">
                                <span class="number"></span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                            <div class="block">
                                <span class="number"></span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                            <div class="block">
                                <span class="number"></span>
                                <p>Lorem ipsum Lorem ipsum Lorem ipsum Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="container">
                            <h2>Vantagens</h2>
                        </div>
                    </div>
                    <div class="formulario">
                        <h3>Contate-nos</h3>
                        <div class="box-formulario">
                            <form action="enviar.php" method="POST">
                                <input type="text" name="nome" placeholder="Digite seu nome">
                                <input type="text" name="email" placeholder="Digite seu e-mail">
                                <input type="text" name="telefone" placeholder="Digite seu contato">
                                <textarea placeholder="Digite sua mensagem aqui..." name="mensagem"></textarea>
                                <input type="submit" value="ENVIAR">
                            </form>
                        </div>
                    </div>
                </div>
                <div id="footer">
                    <h1>&#169; Copyright empresa.com.br - Todos os direitos reservados - Geri</h1>
                </div>
            </body>
        </html>`,
        css: `@import url('https://fonts.googleapis.com/css?family=Roboto:100,400,700,900&display=swap');
    
        *{
            margin:0;
            padding:0;
            box-sizing: border-box;
            font-family:"Helvetica Neue",Helvetica,Arial,sans-serif;
        }
        body{
            margin:0;
            padding:0;
            display: flex;
            flex-direction: column;
        }
        .d-flex{
            display: flex;
            justify-content: center;
            align-items: center;
        }
        .imgWhite{
            filter: invert(31%) sepia(20%) saturate(5%) hue-rotate(320deg) brightness(100%) contrast(92%);
        }
        
        /* Cabeçalho - Header */
        header{
            width:100%;
            display: flex;
            justify-content: space-between;
            padding: 10px 20px;
            position: fixed;
            top: 0;
            background: #fff;
            box-shadow: 0px 3px 6px rgba(0,0,0,.5);
            align-items: center;
        }
        header img{
            width: 100px;
            height: 60px;
        }
        header .container{
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            color: #555;
            font-size: 0.8rem;
        }
        header .container div:first-child{
            justify-content: right;
            flex-direction: row-reverse;
        }
        header .d-flex{
            display: flex;
            align-items: center;
        }
        header .img2{
            width: 30px;
            height: 30px;
            margin-left: 20px;
        }
        
        /* Painel - Banner */
        .painel{
            background:#aaa;
            background-image:url(../img/fundo-cinza.jpg);
            width:100%;
            height:800px;
            background-position:top;
            background-blend-mode:multiply;
            background-attachment:fixed;
            background-size:cover;
            display: flex;
            align-items: center;
            justify-content: center;
            flex-direction: column;
            margin-top: 80px;
        }
        .text-painel{
            color:#fff;
            font-weight:bold;
            font-size:2.7rem;
            width: 70%;
            text-align: center;
        }
        #destaque{
            text-align:center;
        }
        
        /* Quem Somos */
        
        .aboutUs{
            display: flex;
            flex-direction: column;
        }
        
        .aboutUs .container{
            display: flex;
            width: 100%;
            align-items: center;
            justify-content: center;
            min-height: 150px;
            padding: 30px 0;
            text-indent: 30px;
            flex-direction: column;
        }
        
        .aboutUs .container:first-child{
            background: #f39c12;
        }
        
        .aboutUs .container p {
            max-width: 600px;
            padding: 10px;
            text-align: justify;
            line-height: 25px;
            color: #777;
        }
        
        .aboutUs h1{
            color: #fff;
        }
        
        /* Vantagens */
        .vantagens{
            display: flex;
            flex-direction: column-reverse;
        }
        
        .vantagens .container{
            display: flex;
            width: 100%;
            padding: 15px;
            align-items: center;
            justify-content: center;
            min-height: 150px;
        }
        
        .vantagens .container h2{
            color: #fff;
            font-size: 2em;
            font-weight: bold;
        }
        
        .vantagens .container:last-child{
            background:#3498db;
        }
        
        .vantagens .container .block{
            text-align: left;
            margin: 20px;
            display: flex;
            align-items: center;
            width: 300px;
            flex-direction: column;
            box-shadow: 1px 3px 4px rgba(0,0,0,.4);
        }
        
        .vantagens .container .block p{
            max-width: 300px;
            color: #777;
            padding: 25px;
        }
        
        .vantagens .container .block span{
            font-size: 3rem;
            font-weight: bold;
            color: #fff;
            width: 100%;
            background:#3498db;
            text-align: center;
            padding: 5px;
            height: 50px;
        }
        
        /* Contato */
        .formulario{
            background: #2980b9;
            height:auto;
            width:100%;
            color:#fff;
            padding:20px 0 20px 0;
        }
        .formulario h3{
            font-size:25pt;
            margin-top:10px;
        }
        .formulario input[type=text]{
            width:500px;
            height:35px;
            color:#999;
            border:none;
            padding:10px;
            margin:20px;
            outline: none;
        }
        .formulario input[type=submit]{
            padding:15px 100px 15px 100px;
            border:none;
            background:rgba(52, 152, 219,1.0);
            color:#fff;
            cursor:pointer;
        }
        .formulario input[type=submit]:hover{
            background:#f39c12;
        }
        .formulario textarea{
            width:500px;
            height:200px;
            margin:20px;
            border:none;
            padding:10px;
            color:#999;
            margin-top:0px;
            outline: none;
        }
        .box-formulario{
            width:540px;
            margin:0 auto;
            margin-top:30px;
        }
        
        /* Footer */
        #footer{
            width:100%;
            background:#fdfdfd;
            text-align:center;
            color:#555;
            padding:15px 0px 10px;
            font-size:6pt;
        }
        
        /* Mobile */
        @media (max-width:600px){
            header{
                flex-direction: column;
                justify-content: center;
                align-items: center;
            }
            header .container div:first-child{
                justify-content: center;
                margin-top: 25px;
            }
            .painel{
                height: 900px;
            }
            .text-painel{
                margin-top: 250px;
            }
            .aboutUs{
                flex-direction: column;
            }
            .vantagens .container, .aboutUs .container{
                width: 100%;
                min-height: 0px;
                padding: 20px 30px;
            }
            .vantagens .container p, .aboutUs .container p{
                font-size: 1.6rem;
                line-height: 30px;
            }
            .vantagens .container .block p{
                max-width: 500px;
            }
            header, .painel, .formulario, #footer{
                max-width:610px;
            }
            .formulario input[type=text]{
                height: 70px;
                font-size: 2rem;
            }
            .formulario input[type=submit]{
                height: 70px;
                font-size: 2rem;
            }
            .formulario textarea{
                font-size: 2rem;
                height: 500px;
                margin-top: 25px;
            }
            header .img2{
                height:60px;
                width:60px;
            }
        }`
    }
]