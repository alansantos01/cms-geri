export class Produto {
    image: string;
    name: string;
    description: string;
    price: number;
    desconto: number;
    category: string;
}
