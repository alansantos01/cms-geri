import { MissingTranslationStrategy } from '@angular/core';

export class Filial {
    nmFilial: string
    tpFilial: string
    zipCode: string
    address: string
    number: number
    neighborhood: string
    city: string
    state: string
    phone: string
    email: string
}
