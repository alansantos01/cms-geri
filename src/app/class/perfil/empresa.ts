export class Empresa {
    nmFantasia: string
    razaoSocial: string
    ramo: string
    nmrFuncionario: string
    nmrFiliais: string
    tmCompany: string
    zipCode: string
    address: string
    numberLocation: number
    neighborhood: string
    city: string
    state: string
    cnpj: number
}
