import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  public sub_menu: boolean;
  public menu: string;
  public select: string;
  public abas_select: string;
  public modalAtivo: string;
  private username: string = 'alansantos';
  private password: string = '123alan';
  public logado: string;
  public backgroundBody: string;
  public backgroundColorBody: string;
  public sectionAtiva: string;

  //Cores
  public corPrimaria: string;
  public corPrimariaDark: string;
  public corSecundaria: string;
  public corSecundariaDark: string;
  public corTerceraria: string;
  
  constructor() {

  }
  public showSubMenu(identificador){      
    this.sub_menu = true;
    this.menu = identificador;
  }  
  public hideSubMenu(){
    this.sub_menu = false;
    this.menu = "";
  }

  public onselect(valor){
    this.select = valor;
  }
  public abrirModal(modal){
    this.modalAtivo = modal;
  }
  public abas(aba_v){
    this.abas_select = aba_v;
  }
  public background(bgBody){
    this.backgroundBody = bgBody;
  }
  public background_color(bgColorBody){
    this.backgroundColorBody = bgColorBody;
  }
  
  public abrirSection(section){
    this.sectionAtiva = section;
  }
  public nmArquivo(id){
    var alvo = $(id);
    var alvo_next = $(alvo).next();
    var inputNome = $(alvo).prev();
    var inputFicheiro = alvo_next;
    var nome = $(inputFicheiro).val().split("\\").pop();
    $(inputNome).text(nome);
    $(inputNome).html(nome);
  };
}
