import { Component, OnInit, AfterContentChecked } from '@angular/core';
import * as $ from 'jquery';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-menu-lateral',
  templateUrl: './menu-lateral.component.html',
  styleUrls: ['./menu-lateral.component.css']
})
export class MenuLateralComponent implements OnInit, AfterContentChecked {
  
  public sub_menu: boolean;
  public menu: string;
  public modalAtivo: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {

  }

  public show(item) {
    this.generalService.showSubMenu(item);
  }
  public hide() {
    this.generalService.hideSubMenu();
  }
  public onselect(valor){
    this.generalService.onselect(valor);
  }
  ngAfterContentChecked(){
    this.sub_menu = this.generalService.sub_menu;
    this.menu = this.generalService.menu;
    this.modalAtivo = this.generalService.modalAtivo;
  }


}
