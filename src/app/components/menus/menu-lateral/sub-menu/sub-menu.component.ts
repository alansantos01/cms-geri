import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import * as $ from 'jquery';

@Component({
  selector: 'app-sub-menu',
  templateUrl: './sub-menu.component.html',
  styleUrls: ['./sub-menu.component.less']
})
export class SubMenuComponent implements OnInit, AfterContentChecked {

  public sub_menu: boolean;
  public menu: string;
  public select: string;

  constructor(public generalService: GeneralService) { }

  ngOnInit() {
  }
  public onselect(valor){
    this.generalService.onselect(valor);
  }
  ngAfterContentChecked(){
    this.sub_menu = this.generalService.sub_menu;
    this.menu = this.generalService.menu;
    this.select = this.generalService.select;
  }

}
