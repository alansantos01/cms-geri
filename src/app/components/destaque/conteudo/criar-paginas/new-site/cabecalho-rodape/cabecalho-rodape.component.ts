import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-cabecalho-rodape',
  templateUrl: './cabecalho-rodape.component.html',
  styleUrls: ['./cabecalho-rodape.component.scss']
})
export class CabecalhoRodapeComponent implements OnInit, AfterContentChecked {
  public select: string;
  public aba: string;
  public sectionAtiva: string;
  public headerCustom: boolean = true;
  public footerCustom: boolean = true;
  public sombreamento: boolean = true;
  public btnHeader: boolean = true;
  public corHeader: string;
  public corMenuHeader: string;
  public corFontHeader: string;
  public corHoverMenuHeader: string;

  public sombreamentoFooter: boolean = false;
  public btnFooter: boolean = true;
  public corFooter: string;
  public corFontFooter: string;

  //Cores
  public corPrimaria: string;
  public corPrimariaDark: string;
  public corSecundaria: string;
  public corSecundariaDark: string;
  public corTerceraria: string;

  constructor(private generalService: GeneralService) {
  }

  ngOnInit() {
  }

  public triggerSomeEvent_sombreamento(){
    this.sombreamento = !this.sombreamento;
    return;
  }
  public triggerSomeEvent_btnHome(){
    this.btnHeader = !this.btnHeader;
    return;
  }

  public triggerSomeEvent_sombreamentoFooter(){
    this.sombreamentoFooter = !this.sombreamentoFooter;
    return;
  }
  public triggerSomeEvent_btnHomeFooter(){
    this.btnFooter = !this.btnFooter;
    return;
  }

  public mudarCorHeader(){
    var x = (document.getElementById("selectCorHeader")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corHeader = 'transparent'
        break
      case 2:
        this.corHeader = '#fff'
        break
      case 3:
        this.corHeader = this.corPrimaria
        break
      case 4:
        this.corHeader = this.corPrimariaDark
        break
      case 5:
        this.corHeader = this.corSecundaria
        break
      case 6:
        this.corHeader = this.corSecundariaDark
        break
      case 7:
        this.corHeader = this.corTerceraria
        break
    }
  }
  public mudarCorMenuHeader(){
    var x = (document.getElementById("selectCorMenuHeader")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corMenuHeader = 'transparent'
        break
      case 2:
        this.corMenuHeader = '#fff'
        break
      case 3:
        this.corMenuHeader = this.corPrimaria
        break
      case 4:
        this.corMenuHeader = this.corPrimariaDark
        break
      case 5:
        this.corMenuHeader = this.corSecundaria
        break
      case 6:
        this.corMenuHeader = this.corSecundariaDark
        break
      case 7:
        this.corMenuHeader = this.corTerceraria
        break
    }
  }
  public mudarCorHoverMenuHeader(){
    var x = (document.getElementById("selectCorHoverMenuHeader")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corHoverMenuHeader = 'transparent'
        break
      case 2:
        this.corHoverMenuHeader = '#fff'
        break
      case 3:
        this.corHoverMenuHeader = this.corPrimaria
        break
      case 4:
        this.corHoverMenuHeader = this.corPrimariaDark
        break
      case 5:
        this.corHoverMenuHeader = this.corSecundaria
        break
      case 6:
        this.corHoverMenuHeader = this.corSecundariaDark
        break
      case 7:
        this.corHoverMenuHeader = this.corTerceraria
        break
    }
  }
  public mudarCorFontHeader(){
    var x = (document.getElementById("selectCorFontHeader")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corFontHeader = '#fff'
        break
      case 2:
        this.corFontHeader = '#bbb'
        break
      case 3:
        this.corFontHeader = "#999"
        break
      case 4:
        this.corFontHeader = "#555"
        break
      case 5:
        this.corFontHeader = "#222"
        break
      case 6:
        this.corFontHeader = "#000"
        break
    }
  }

  public mudarCorFooter(){
    var x = (document.getElementById("selectCorFooter")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corFooter = 'transparent'
        break
      case 2:
        this.corFooter = '#fff'
        break
      case 3:
        this.corFooter = this.corPrimaria
        break
      case 4:
        this.corFooter = this.corPrimariaDark
        break
      case 5:
        this.corFooter = this.corSecundaria
        break
      case 6:
        this.corFooter = this.corSecundariaDark
        break
      case 7:
        this.corFooter = this.corTerceraria
        break
    }
  }
  public mudarCorFontFooter(){
    var x = (document.getElementById("selectCorFontFooter")) as HTMLSelectElement;
    var y = x.selectedIndex
    switch(y){
      case 1:
        this.corFontFooter = '#fff'
        break
      case 2:
        this.corFontFooter = '#bbb'
        break
      case 3:
        this.corFontFooter = "#999"
        break
      case 4:
        this.corFontFooter = "#555"
        break
      case 5:
        this.corFontFooter = "#222"
        break
      case 6:
        this.corFontFooter = "#000"
        break
    }
  }

  public triggerSomeEvent_headerCustom() {
    this.headerCustom = !this.headerCustom;
    return;
  }
  public triggerSomeEvent_footerCustom() {
    this.footerCustom = !this.footerCustom;
    return;
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = '';
    } else {
      this.generalService.sectionAtiva = section;
    }
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.sectionAtiva = this.generalService.sectionAtiva;
    this.corPrimaria = this.generalService.corPrimaria;
    this.corPrimariaDark = this.generalService.corPrimariaDark;
    this.corSecundaria = this.generalService.corSecundaria;
    this.corSecundariaDark = this.generalService.corSecundariaDark;
    this.corTerceraria = this.generalService.corTerceraria;
  }
}