import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CabecalhoRodapeComponent } from './cabecalho-rodape.component';

describe('CabecalhoRodapeComponent', () => {
  let component: CabecalhoRodapeComponent;
  let fixture: ComponentFixture<CabecalhoRodapeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CabecalhoRodapeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CabecalhoRodapeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
