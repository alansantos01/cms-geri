import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';

@Component({
  selector: 'app-config',
  templateUrl: './config.component.html',
  styleUrls: ['./config.component.scss']
})
export class ConfigComponent implements OnInit, AfterContentChecked {
  public select: string;
  public aba: string;
  public modalAtivo: string;
  public sectionAtiva: string;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = '';
    } else {
      this.generalService.sectionAtiva = section;
    }
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.modalAtivo = this.generalService.modalAtivo;
    this.sectionAtiva = this.generalService.sectionAtiva;
  }

}
