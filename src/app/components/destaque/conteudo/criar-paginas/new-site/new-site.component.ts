import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import {FormControl, Validators, FormGroup, FormBuilder} from "@angular/forms";
import * as $ from 'jquery';

@Component({
  selector: 'app-new-site',
  templateUrl: './new-site.component.html',
  styleUrls: ['./new-site.component.scss']
})
export class NewSiteComponent implements OnInit, AfterContentChecked {
  public select: string;
  public aba: string;
  public modalAtivo: string;
  public sectionAtiva: string;
  private formulario: FormGroup;
  public descontoDisable: boolean = true;

  constructor(private generalService: GeneralService, private formBuilder: FormBuilder) { 
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
      name: [null, [Validators.minLength(4), Validators.required, Validators.maxLength(100)]],
      username: [null, [Validators.minLength(6), Validators.required, Validators.maxLength(25)]],
      telefone: [null, [Validators.minLength(8), Validators.required, Validators.maxLength(13)]],
      text: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(50)]],
      description: [null, [Validators.minLength(1), Validators.required, Validators.maxLength(150)]],
      senha: [null, [Validators.minLength(6), Validators.required, Validators.maxLength(20)]]
    })
  }

  verificaValidTouched(campo){
    return (this.formulario.get(campo).invalid && (this.formulario.get(campo).touched || this.formulario.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }
  ngOnInit() {
  }
  public triggerSomeEvent() {
    this.descontoDisable = !this.descontoDisable;
    return;
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public previewImg(imagem, input) {
    this.generalService.previewImg(imagem, input);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = '';
    } else {
      this.generalService.sectionAtiva = section;
    }
  }
  public alert(name){
    alert(name + " atualizado com sucesso!");
    this.formulario.reset();
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.modalAtivo = this.generalService.modalAtivo;
    this.sectionAtiva = this.generalService.sectionAtiva;
  }
}
