import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import {FormControl, Validators, FormGroup, FormBuilder} from "@angular/forms";
import { CadastrarDadosService } from 'src/app/services/cadastrarDados/cadastrar-dados.service';

@Component({
  selector: 'app-template-choice',
  templateUrl: './template-choice.component.html',
  styleUrls: ['./template-choice.component.scss']
})
export class TemplateChoiceComponent implements OnInit {
  public colorDisable: boolean = true;
  public aboutUsDisable: boolean = true;
  public blogDisable: boolean = true;
  public select: string;
  public aba: string;
  public modalAtivo: string;
  public sectionAtiva: string;
  private benefitsForm: FormGroup;
  private mapForm: FormGroup;
  private seoForm: FormGroup;
  public descontoDisable: boolean = true;
  mapDisable: boolean;

  constructor(private generalService: GeneralService, private formBuilder: FormBuilder, private dataService: CadastrarDadosService) {
    this.benefitsForm = this.formBuilder.group({
      benefit1: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(50)]],
      benefit2: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(50)]],
      benefit3: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(50)]]
    })
    this.mapForm = this.formBuilder.group({
      map: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(150)]]
    })
    this.seoForm = this.formBuilder.group({
      title: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(70)]],
      description: [null, [Validators.minLength(10), Validators.required, Validators.maxLength(250)]],
      tags: [null, [Validators.minLength(3), Validators.required, Validators.maxLength(100)]]
    })
   }

  public triggerSomeEvent_colorPrimaria(){var x = document.getElementById("cor-primaria") as HTMLSelectElement; this.generalService.corPrimaria = x.value; console.log(x.value)}
  public triggerSomeEvent_colorPrimariaDark(){var x = document.getElementById("cor-primariaDark") as HTMLSelectElement; this.generalService.corPrimariaDark = x.value}
  public triggerSomeEvent_colorSecundaria(){var x = document.getElementById("cor-secundaria") as HTMLSelectElement; this.generalService.corSecundaria = x.value}
  public triggerSomeEvent_colorSecundariaDark(){var x = document.getElementById("cor-secudariaDark") as HTMLSelectElement; this.generalService.corSecundariaDark = x.value}

  verificaValidTouched(campo){
    return (this.benefitsForm.get(campo).invalid && (this.benefitsForm.get(campo).touched || this.benefitsForm.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }
  verificaValidTouched2(campo){
    return (this.mapForm.get(campo).invalid && (this.mapForm.get(campo).touched || this.mapForm.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }
  verificaValidTouched3(campo){
    return (this.seoForm.get(campo).invalid && (this.seoForm.get(campo).touched || this.seoForm.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }
  ngOnInit() {
  }

  public triggerSomeEvent_map() {
    this.mapDisable = !this.mapDisable;
  }
  public triggerSomeEvent_aboutUs() {
    this.aboutUsDisable = !this.aboutUsDisable;
  }
  public triggerSomeEvent() {
    this.descontoDisable = !this.descontoDisable;
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = '';
    } else {
      this.generalService.sectionAtiva = section;
    }
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v);
  }
  public saveData() {
    this.dataService.template.colors.primary = this.generalService.corPrimaria;
    this.dataService.template.colors.primaryDark = this.generalService.corPrimariaDark;
    this.dataService.template.colors.secondary = this.generalService.corSecundaria;
    this.dataService.template.colors.secondaryDark = this.generalService.corSecundariaDark;

    this.dataService.template.templateConfig.aboutUs = true;
    this.dataService.template.templateConfig.map = true;
    this.dataService.template.templateConfig.templateChoice = 0;
    
    this.dataService.template.aboutUsConfig.text = '';

    this.dataService.template.benefits.benefit1 = this.benefitsForm.get('benefit1').value;
    this.dataService.template.benefits.benefit2 = this.benefitsForm.get('benefit2').value;
    this.dataService.template.benefits.benefit3 = this.benefitsForm.get('benefit3').value;
    
    this.dataService.template.mapLocation.url = this.mapForm.get('map').value == undefined ? '' : this.mapForm.get('map').value == null ? '' : this.mapForm.get('map').value;

    this.dataService.template.socialMedias.facebook = '';
    this.dataService.template.socialMedias.instagram = '';
    this.dataService.template.socialMedias.officialSite = '';
    this.dataService.template.socialMedias.twitter = '';
    this.dataService.template.socialMedias.blog = '';
    this.dataService.template.socialMedias.youtube = '';

    this.dataService.template.metatags.title = this.seoForm.get('title').value;
    this.dataService.template.metatags.description = this.seoForm.get('description').value;
    this.dataService.template.metatags.tags = this.seoForm.get('tags').value;
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.aba = this.generalService.abas_select;
    this.modalAtivo = this.generalService.modalAtivo;
    this.sectionAtiva = this.generalService.sectionAtiva;
  }
}
