import { Component, OnInit } from '@angular/core';
import {Validators, FormGroup, FormBuilder} from "@angular/forms";

//Service
import { CadastrarPerfilService } from 'src/app/services/cadastrar-perfil.service';
import { BuscaCepService } from 'src/app/services/busca-cep.service';
import { GeneralService } from 'src/app/principal/general.service';
import { CadastrarDadosService } from 'src/app/services/cadastrarDados/cadastrar-dados.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.scss']
})
export class PerfilComponent implements OnInit {
  public select: string
  public aba: string
  public modalAtivo: string
  public sectionAtiva: string
  private formulario: FormGroup
  public descontoDisable: boolean = true
  public filialDisable: boolean = true

  //Objetos
  public objPerfil: object
  public cepPlus: any[]

  constructor(private generalService: GeneralService, 
    private formBuilder: FormBuilder, 
    private cadastrarPerfil: CadastrarPerfilService, 
    private buscaCep: BuscaCepService,
    private dataService: CadastrarDadosService) { 
    this.formulario = this.formBuilder.group({
      email: [null, [Validators.email, Validators.required]],
      name: [null, [Validators.minLength(4), Validators.required, Validators.maxLength(100)]],
      telefone: [null, [Validators.minLength(7), Validators.required]],
      cep: [null, [Validators.required]],
      address: [null, [Validators.minLength(2), Validators.required, Validators.maxLength(10)]],
      number: [null, [Validators.required, Validators.maxLength(10)]],
      complemento: [null, [Validators.required, Validators.maxLength(10)]],
      bairro: [null, [Validators.minLength(2), Validators.required, Validators.maxLength(10)]],
      city: [null, [Validators.minLength(2), Validators.required, Validators.maxLength(10)]],
      uf: [null, [Validators.required]]
    })
  }

  verificaValidTouched(campo){
    return (this.formulario.get(campo).invalid && (this.formulario.get(campo).touched || this.formulario.get(campo).dirty) ) ? 'has-erro has-feedback' : ''
  }
  ngOnInit() {
  }
  public triggerSomeEvent_filial() {
    this.filialDisable = !this.filialDisable
    return;
  }
  public abrirAba(aba_v) {
    this.generalService.abas(aba_v)
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal)
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = ''
    } else {
      this.generalService.sectionAtiva = section
    }
  }
  
  public saveData(){
    this.dataService.perfil.company.name = this.formulario.get('name').value;
    this.dataService.perfil.company.phone = this.formulario.get('phone').value;
    this.dataService.perfil.company.email = this.formulario.get('email').value;
    this.dataService.perfil.company.cep = this.formulario.get('cep').value;
    this.dataService.perfil.company.address = this.formulario.get('address').value;
    this.dataService.perfil.company.number = this.formulario.get('number').value;
    this.dataService.perfil.company.complemento = this.formulario.get('complemento').value;
    this.dataService.perfil.company.neighborhood = this.formulario.get('bairro').value;
    this.dataService.perfil.company.city = this.formulario.get('city').value;
    this.dataService.perfil.company.state = this.formulario.get('uf').value;
  }

  public verificarCep() {
    var nmCep = this.formulario.get('cep').value.replace(/\-/g,"");
    if(nmCep == null || nmCep == ""){
      alert("Digite um CEP válido")
    } else if(nmCep.length != 8){
      alert("Digite um CEP válido")
    } else {
      this.buscaCep.vlrCep(nmCep)
      this.buscaCep.getBuscaCep().subscribe(
        response => {
          this.cepPlus = response
        }
      )
    }
    console.log(this.cepPlus)
  }
  ngAfterContentChecked(){
    this.select = this.generalService.select
    this.aba = this.generalService.abas_select
    this.modalAtivo = this.generalService.modalAtivo
    this.sectionAtiva = this.generalService.sectionAtiva
    this.objPerfil = this.cadastrarPerfil.perfilObj
  }
}
