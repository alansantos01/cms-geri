import { Component, OnInit, AfterContentChecked } from '@angular/core';
import { GeneralService } from 'src/app/principal/general.service';
import {FormControl, Validators, FormGroup, FormBuilder} from "@angular/forms";

//Classes
import { Produto } from 'src/app/class/produto';
import { CadastrarCardapioService } from 'src/app/services/cadastrar-cardapio.service';

@Component({
  selector: 'app-cardapio',
  templateUrl: './cardapio.component.html',
  styleUrls: ['./cardapio.component.scss']
})
export class CardapioComponent implements OnInit, AfterContentChecked {
  public select: string
  public modalAtivo: string
  public sectionAtiva: string
  public descontoDisable: boolean = true
  public produtos: Produto = new Produto
  public formularioCardapio: FormGroup

  constructor(private generalService: GeneralService, private formBuilder: FormBuilder, private cadastrarCardapio: CadastrarCardapioService) { 
    this.formularioCardapio = this.formBuilder.group({
      nameProd: [null, [Validators.minLength(4), Validators.required, Validators.maxLength(100)]],
      priceProd: [null, [Validators.required, Validators.maxLength(10)]],
      descontoProd: [null, [Validators.required, Validators.maxLength(10)]],
      imageProd: [null, [Validators.required, Validators.maxLength(10)]],
      categoryProd: [null, [Validators.required, Validators.maxLength(10)]],
      descriptionProd: [null, [Validators.minLength(1), Validators.required, Validators.maxLength(150)]]
    })
  }
  verificaValidTouched(campo){
    return (this.formularioCardapio.get(campo).invalid && (this.formularioCardapio.get(campo).touched || this.formularioCardapio.get(campo).dirty) ) ? 'has-erro has-feedback' : '';
  }

  ngOnInit() {
  }
  public cadastroProduto(){
    this.cadastrarCardapio.cadastrarProduto(this.produtos)
    this.formularioCardapio.reset()
    console.log(this.cadastrarCardapio.cardapio)
  }
  public triggerSomeEvent() {
    this.descontoDisable = !this.descontoDisable;
    return;
  }
  public nmArquivo(file) {
    this.generalService.nmArquivo(file);
  }
  public previewImg(imagem, input) {
    this.generalService.previewImg(imagem, input);
  }
  public abrirModal(modal) {
    this.generalService.abrirModal(modal);
  }
  public abrirSection(section){
    if(this.generalService.sectionAtiva == section){
      this.generalService.sectionAtiva = '';
    } else {
      this.generalService.sectionAtiva = section;
    }
  }

  ngAfterContentChecked(){
    this.select = this.generalService.select;
    this.modalAtivo = this.generalService.modalAtivo;
    this.sectionAtiva = this.generalService.sectionAtiva;
  }
}
