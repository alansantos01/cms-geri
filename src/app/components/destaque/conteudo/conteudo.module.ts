import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NbButtonModule, NbCardModule } from '@nebular/theme';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


import { WhiteLabelComponent } from './criar-paginas/white-label/white-label.component';
import { LandingPagesComponent } from './criar-paginas/landing-pages/landing-pages.component';
import { StaticPagesComponent } from './criar-paginas/static-pages/static-pages.component';
import { ConfiguracoesComponent } from './configuracoes-page/configuracoes/configuracoes.component';
import { ConteudoComponent } from './conteudo.component';
import { MenuModule } from '../../menus/menu.module';
import { ModaisModule } from '../../../modais/modais.module';
import { NewSiteComponent } from './criar-paginas/new-site/new-site.component';
import { CabecalhoRodapeComponent } from './criar-paginas/new-site/cabecalho-rodape/cabecalho-rodape.component';
import { TemplateChoiceComponent } from './criar-paginas/new-site/template-choice/template-choice.component';
import { CardapioComponent } from './criar-paginas/new-site/cardapio/cardapio.component';
import { PerfilComponent } from './criar-paginas/new-site/perfil/perfil.component';
import { ConfigComponent } from './criar-paginas/new-site/config/config.component';
import { NgxMaskModule } from 'ngx-mask'


@NgModule({
  declarations: [
    WhiteLabelComponent,
    LandingPagesComponent,
    StaticPagesComponent,
    ConfiguracoesComponent,
    ConteudoComponent,
    NewSiteComponent,
    CabecalhoRodapeComponent,
    TemplateChoiceComponent,
    CardapioComponent,
    PerfilComponent,
    ConfigComponent
  ],
  exports: [
    ConteudoComponent
  ],
  imports: [
    CommonModule,
    NbCardModule,
    NbButtonModule,
    MenuModule,
    ModaisModule,
    FormsModule,
    ReactiveFormsModule,
    NgxMaskModule.forRoot()
  ]
})
export class ConteudoModule { }
