import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Components
import { AppComponent } from './app.component';


//Services
import { GeneralService } from './principal/general.service';

//Routes
import { RoutesRoutingModule } from './routes/routes-routing.module';

//Modules
import { LoginComponent } from './login/login.component';
import { SystemModule } from './components/system.module';
import { SignService } from './login/sign.service';
import { NbThemeModule } from '@nebular/theme';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    SystemModule,
    RoutesRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NbThemeModule.forRoot(),
    HttpClientModule
  ],
  providers: [GeneralService, SignService],
  bootstrap: [AppComponent]
})
export class AppModule { }
