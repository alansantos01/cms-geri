import { TestBed } from '@angular/core/testing';

import { CadastrarPerfilService } from './cadastrar-perfil.service';

describe('CadastrarPerfilService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastrarPerfilService = TestBed.get(CadastrarPerfilService);
    expect(service).toBeTruthy();
  });
});
