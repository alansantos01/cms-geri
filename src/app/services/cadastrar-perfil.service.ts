import { Injectable } from '@angular/core';
import { Login } from '../class/perfil/login';
import { Contato } from '../class/perfil/contato';
import { Filial } from '../class/perfil/filial';
import { Empresa } from '../class/perfil/empresa';
import { Responsavel } from '../class/perfil/responsavel';

@Injectable({
  providedIn: 'root'
})
export class CadastrarPerfilService {
  public phones: Array<object> = []
  public emails: Array<object> = []
  public filiais: Array<object> = []
  public logins: Array<object> = []
  public perfilObj = {
    numberPhones: this.phones,
    addressEmail: this.emails,
    filial: this.filiais,
    login: this.logins,
    nmFantasia: '',
    razaoFantasia: '',
    nmResponsible: '',
    zipCode: '',
    address: '',
    numberLocation: 0,
    neighborhood: '',
    city: '',
    state: '',
    cnpj: 0,
    cpf: 0,
    ramo: '',
    nmrEmployee: '',
    nmrFiliais: '',
    tmCompany: ''
  }

  //Classes
  public loginClass: Login = new Login
  public contatoClass: Contato = new Contato
  public filialClass: Filial = new Filial
  public empresaClass: Empresa = new Empresa
  public responsavelClass: Responsavel = new Responsavel

  constructor() { }

  public addPhone(phones: Contato){
    let numberPhones = {
      numberP: phones.phone,
      tpPhone: phones.tpPhone
    }
    this.phones.push(numberPhones)
  }
  public addEmail(emails: Contato){
    let addressEmail = {
      addressE: emails.email,
      tpEmail: emails.typeEmail
    }
    this.emails.push(addressEmail)
  }
  public addFilial(filiais: Filial){
    let filial = {
      nmFilial: filiais.nmFilial,
      tpFilial: filiais.tpFilial,
      zipCode: filiais.zipCode,
      address: filiais.address,
      number: filiais.number,
      neighborhood: filiais.neighborhood,
      city: filiais.city,
      state: filiais.state,
      phone: filiais.phone,
      email: filiais.email
    }
    this.filiais.push(filial)
  }
  public addLogin(logins: Login){
    let login = {
      site: logins.site,
      systemMng: logins.systemMng,
      username: logins.username,
      nmComplete: logins.nmComplete,
      password: logins.password,
      nvAcess: logins.nvAcess,
      timeUser: logins.timeUser
    }
    this.logins.push(login)
  }
  public addEmpresa(empresa: Empresa){
    this.perfilObj.nmFantasia = empresa.nmFantasia
    this.perfilObj.razaoFantasia = empresa.razaoSocial
    console.log(this.perfilObj)
  }
  public addResponsavel(responsable: Responsavel){
    this.perfilObj.nmResponsible = responsable.nome
  }
  public addAddressCompany(addressComplete: Empresa){
    this.perfilObj.zipCode = addressComplete.zipCode
    this.perfilObj.address = addressComplete.address
    this.perfilObj.numberLocation = addressComplete.numberLocation
    this.perfilObj.neighborhood = addressComplete.neighborhood
    this.perfilObj.city = addressComplete.city
    this.perfilObj.state = addressComplete.state
  }
  public addDocs(docCompany: Empresa, docPersonal: Responsavel){
    this.perfilObj.cnpj = docCompany.cnpj,
    this.perfilObj.cpf = docPersonal.cpf
  }
  public addTypeCompany(tpCompany: Empresa){
    this.perfilObj.ramo = tpCompany.ramo,
    this.perfilObj.nmrEmployee = tpCompany.nmrFuncionario,
    this.perfilObj.nmrFiliais = tpCompany.nmrFiliais,
    this.perfilObj.tmCompany = tpCompany.tmCompany
  }
  // public objPerfil(){
  //   this.perfilObj = {
  //     filial: this.filiais,
  //     email: this.emails,
  //     phone: this.phones,
  //     login: this.logins
  //   }
  // }
}