import { TestBed } from '@angular/core/testing';

import { CadastrarCardapioService } from './cadastrar-cardapio.service';

describe('CadastrarCardapioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastrarCardapioService = TestBed.get(CadastrarCardapioService);
    expect(service).toBeTruthy();
  });
});
