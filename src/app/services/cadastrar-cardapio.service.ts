import { Injectable } from '@angular/core';
import { Produto } from '../class/produto';
import * as $ from 'jquery';

@Injectable({
  providedIn: 'root'
})
export class CadastrarCardapioService {
  public cardapio: Array<object> = []

  constructor() { }
  public cadastrarProduto(produto: Produto){
    let addCardapio = {
      image: produto.image,
      name: produto.name,
      description: produto.description,
      price: produto.price,
      desconto: produto.desconto,
      category: produto.category
    }
    if(produto.desconto === null || produto.desconto === undefined){
      addCardapio.desconto = 0
    }
    this.cardapio.push(addCardapio)
  }
}
