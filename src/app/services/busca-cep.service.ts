import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class BuscaCepService {

  private cep: string
  constructor(private http: HttpClient) { }

  public vlrCep(valorCep){
    this.cep = valorCep
  }

  public getBuscaCep():Observable<any[]>{
    let url = "https://viacep.com.br/ws/" + this.cep + "/json/"
    return this.http.get<any[]>(url);
  }
}
