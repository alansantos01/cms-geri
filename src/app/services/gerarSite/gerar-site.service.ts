import { Injectable } from '@angular/core';
import { CadastrarDadosService } from '../cadastrarDados/cadastrar-dados.service';
import { template1 } from 'src/app/class/templates/template1';

@Injectable({
  providedIn: 'root'
})
export class GerarSiteService {
  public templates = {
    default: template1[0],
    default2: template1[1],
  };

  constructor(public dataService: CadastrarDadosService) { }
}
