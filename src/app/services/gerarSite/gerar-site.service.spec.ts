import { TestBed } from '@angular/core/testing';

import { GerarSiteService } from './gerar-site.service';

describe('GerarSiteService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: GerarSiteService = TestBed.get(GerarSiteService);
    expect(service).toBeTruthy();
  });
});
