import { TestBed } from '@angular/core/testing';

import { CadastrarDadosService } from './cadastrar-dados.service';

describe('CadastrarDadosService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CadastrarDadosService = TestBed.get(CadastrarDadosService);
    expect(service).toBeTruthy();
  });
});
