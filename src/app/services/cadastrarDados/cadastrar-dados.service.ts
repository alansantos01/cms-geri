import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CadastrarDadosService {
  public template = {
    colors: {
      primary: '',
      primaryDark: '',
      secondary: '',
      secondaryDark: ''
    },
    templateConfig: {
      aboutUs: false,
      map: false,
      templateChoice: 0
    },
    benefits: {
      benefit1: '',
      benefit2: '',
      benefit3: ''
    },
    aboutUsConfig: {
      text: ''
    },
    mapLocation: {
      url: ''
    },
    socialMedias: {
      facebook: '',
      instagram: '',
      officialSite: '',
      twitter: '',
      blog: '',
      youtube: ''
    },
    metatags: {
      title: '',
      description: '',
      tags: ''
    }
  }
  public perfil = {
    company: {
      name: '',
      phone: '',
      email: '',
      cep: '',
      address: '',
      number: '',
      complemento: '',
      neighborhood: '',
      city: '',
      state: ''
    }
  }
  public configs = {
    logo: '',
    favicon: ''
  }

  constructor() { }
}
